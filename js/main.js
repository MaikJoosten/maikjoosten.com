var body,
    metaData = {},
    metaTitle,
    metaDescription,
    metaOGsite_name,
    metaOGtitle,
    metaOGdescription,
    contentData = {},
    contentDataLength = 0,
    contentJSON = 'content.json',
    languages = ['en', 'de', 'nl'],
    languagesLength = languages.length,
    languageFadeTime = 200,
    defaultLang = 'en',
    language,
    languageSpan,
    sections = null,
    sectionsHolder,
    sectionIndex,
    sectionIndexActive,
    sectionIndexDiv,
    scrollTop = 0,
    newScrollTop = 0,
    scrollTimeoutID = 0,
    scrollToNewAnimationDuration = 800,
    scrollToNewAnimationBusy = false,
    scrollDelta = 0,
    slideshowAnimationDuration = 600,
    slideshowTimeout = 0,
    slideshowTimeoutDuration = 4000,
    slideDefaultHeight = 422,
    slideDefaultWidth = 750,
    mobileViewport = 640,
    isMobile = false,
    midMidWidth = 0,
    windowHeight = 0,
    windowWidth = 0;

$(document).ready(function() {

    // Cache jQuery objects
    body = $('html, body');
    metaTitle = $('title');
    metaDescription = $('meta[name="description"]');
    metaOGsite_name = $('meta[property="og:site_name"]');
    metaOGtitle = $('meta[property="og:title"]');
    metaOGdescription = $('meta[property="og:description"]');
    language = $('#language');
    sectionsHolder = $('#sections');
    sectionIndex = $('#sectionIndex');

    // Load the content
    loadContent();

    // Browse through sections using the cursor keys
    $(window).on('keydown', function(e) {
        switch(e.keyCode) {

            // Up
            case 38:
                scrollToNewSection(e, -1);
                break;

            // Down
            case 40: case 32:
                scrollToNewSection(e, 1);
                break;
        }
    });

    // Stay on top of those resize events
    $(window).on('resize', viewportResize);
    viewportResize();

    if(isMobile) {
        $(window).on('scroll', updateSectionIndex);

    // Keep screen filled with a single section
    } else {
        $(window).on('mousewheel DOMMouseScroll MozMousePixelScroll', function(e) {
            var e = e.originalEvent ? e.originalEvent : e,
                delta = e.wheelDelta ? e.wheelDelta : e.detail ? -e.detail : 0;

            if(scrollToNewAnimationBusy) return false;

            scrollDelta += delta;

            if(Math.abs(scrollDelta) < 100) {
                return false;

            } else {
                scrollDelta = 0;
            }

            if(delta < 0) {
                scrollToNewSection(e, 1);
                return e.preventDefault() && false;

            } else if (delta > 0) {
                scrollToNewSection(e, -1);
                return e.preventDefault() && false;
            }
        });
    }

    // Stop slideshow animation when window loses focus
    $(window).on('focus', function() {
        $('body').addClass('focus');
    });
    $(window).on('blur', function() {
        $('body').removeClass('focus');
    });
    $('body').addClass('focus');
});

function viewportResize() {
    windowHeight = $(window).height();
    windowWidth = $(window).width();
    midMidWidth = windowWidth * 0.7;
    if(midMidWidth > slideDefaultWidth) midMidWidth = slideDefaultWidth;

    isMobile = (windowWidth < mobileViewport);

    if(sections !== null) setSlideHeight();
}

function updateLanguage(lang) {

    // Set new language
    currentLang = lang;

    // Update meta data
    updateMetaLanguage('title', metaTitle, true);
    updateMetaLanguage('title', metaOGsite_name);
    updateMetaLanguage('title', metaOGtitle);
    updateMetaLanguage('text', metaDescription);
    updateMetaLanguage('text', metaOGdescription);

    // Update language specific content
    for(var i = 0; i < contentDataLength; i++) {
        var currentSection = sections.eq(i);

        updateElementLanguage(i, 'title', $('h2', currentSection));
        updateElementLanguage(i, 'text', $('p', currentSection));
        updateElementLanguage(i, 'link', $('.link a', currentSection));
    }
}

function updateMetaLanguage(key, elem, html) {
    if(typeof metaData[key] !== 'undefined') {
        if(html === true) {
            elem.html(typeof metaData[key][currentLang] !== 'undefined' ? metaData[key][currentLang] : metaData[key][defaultLang]);

        } else {
            elem.attr('content', typeof metaData[key][currentLang] !== 'undefined' ? metaData[key][currentLang] : metaData[key][defaultLang]);
        }
    }
}

function updateElementLanguage(i, key, elem) {
    if(typeof contentData[i][key] !== 'undefined') {
        var newContent = '';

        if(key === 'link') {
            newContent = typeof contentData[i][key]['label'][currentLang] !== 'undefined' ? contentData[i][key]['label'][currentLang] : contentData[i][key]['label'][defaultLang];

        } else {
            newContent = typeof contentData[i][key][currentLang] !== 'undefined' ? contentData[i][key][currentLang] : contentData[i][key][defaultLang];
        }

        // Update section index title attributes
        if(key === 'title') {
            sectionIndexDiv.eq(i).find('div').html($('<p>' + newContent + '</p>').text());
        }

        if(newContent != elem.html()) {
            elem.fadeOut(languageFadeTime, function() {
                $(this).html(newContent).fadeIn(languageFadeTime, function() {
                    $(this).removeAttr('style')
                });
            });
        }
    }
}

function updateSectionIndex() {
    if(isMobile) {
        sections.each(function() {
            var t = $(this),
                i = $('.images', t),
                scrollTop = $(window).scrollTop();
                iPos = i.offset().top + i.height();

            if(scrollTop < iPos && iPos < (scrollTop + windowHeight)) {
                t.addClass('focus');

            } else {
                t.removeClass('focus');
            }
        });
        clearInterval(slideshowTimeout);
        slideshowTimeout = setTimeout(slideshowNext, 1000);

    } else {
        sectionIndexActive = newScrollTop / windowHeight;
        sections.removeClass('focus').eq(sectionIndexActive).addClass('focus');
        sectionIndexDiv.removeClass('active').eq(sectionIndexActive).addClass('active');
    }
}

function loadContent() {
    $.getJSON(contentJSON).done(function(data) {
        metaData = data.meta;
        contentData = data.sections;
        contentDataLength = contentData.length;
        buildContent();

    }).error(function(e) {
        alert('uh oh! ' + e);
    });
}

function buildContent() {
    for(var i = 0; i < contentDataLength; i++) {
        var newSection = $('<section><div class="midmid"></div></section>'),
            midMid = $('.midmid', newSection);

        // Title
        if(typeof contentData[i]['title'] !== 'undefined') {
            midMid.append('<h2></h2>');
        }

        // Images
        if(typeof contentData[i]['images'] !== 'undefined') {
            var imagesLength = contentData[i]['images'].length,
                images = $('<div class="images' + (imagesLength === 1 ? ' single' : '') + '"></div>'),
                j = imagesLength - 1;

            for(; j >= 0; j--) {
                images.append('<img src="img/' + contentData[i]['images'][j] + '"' + (imagesLength > 1 && j == 0 ? ' class="current"' : '') + '>');
            }

            midMid.append(images);
        }

        // Tags
        if(typeof contentData[i]['tags'] !== 'undefined') {
            var j = 0,
                tags = $('<div class="tags"></div>'),
                tagsLength = contentData[i]['tags'].length;

            for(; j < tagsLength; j++) {
                tags.append(' [<span>' + contentData[i]['tags'][j] + '</span>] ');
            }
            midMid.append(tags);
        }

        // Text
        if(typeof contentData[i]['text'] !== 'undefined') {
            midMid.append('<p></p>');
        }

        // Link
        if(typeof contentData[i]['link'] !== 'undefined' && typeof contentData[i]['link']['href'] !== 'undefined') {
            midMid.append('<div class="link"><a href="http://' + contentData[i]['link']['href'] + '" target="_blank"></a></div>');
        }
        sectionsHolder.append(newSection);
    }

    // Cache sections
    sections = $('section');

    // Add language choices
    // language.append('<span class="active">' + currentLang + '</span>');
    // for(i = 0; i < languagesLength; i++) {
    //     if(languages[i] !== currentLang) {
    //         language.append('<span>' + languages[i] + '</span>');
    //     }
    // }
    // languageSpan = $('span', language);

    // Switch language
    $('span', language).on('click', function(e) {
        var t = $(this),
            newLang = t.html();

        languageSpan.not(t).removeClass('active');
        t.addClass('active');

        if(newLang !== currentLang) {
            updateLanguage(newLang);
        }
    });

    // Add section index
    for(i = 0; i < contentDataLength; i++) {
        sectionIndex.append('<div><span></span><div></div></div>');
    }
    sectionIndexDiv = $('> div', sectionIndex);

    // Click on section index to go to item
    sectionIndexDiv.on('click', function(e) {
        scrollToNewSection(e, sectionIndexDiv.index(this) - sectionIndexActive);
    });

    // Fill content
    updateLanguage(currentLang);

    // Replace all image for HiDPI screens
    if(!isMobile && window.devicePixelRatio > 1) {
        $('img').each(function() {
            var t = $(this),
                newSrc = t.attr('src').replace(/\.([^\.]*)$/, '@2x.$1');

            t.attr('src', newSrc);
        });
    }

    // Set correct height for slides
    setSlideHeight();

    setTimeout(updateSectionIndex, 500);
}

function slideshowNext() {
    clearInterval(slideshowTimeout);
    slideshowTimeout = setTimeout(slideshowNext, slideshowTimeoutDuration);
    if(!$('body').hasClass('focus')) return false;

    sections.filter('.focus').each(function() {
        var t = $(this),
            images = $('img', t),
            l = images.length;

        if(l > 1) {
            var c = images.filter('.current'),
                i = images.index(c),
                n;

            i--;
            if(i < 0) {
                i = l - 1;
            }
            n = images.eq(i);


            // Mobile fades
            if(isMobile) {
                n.addClass('next').css({
                    opacity: 1
                });
                c.velocity('stop').velocity({
                    opacity: 0
                }, {
                    complete: function() {
                        n.removeClass('next').addClass('current');
                        c.removeClass('current').css({
                            opacity: 1
                        });
                    },
                    duration: slideshowAnimationDuration,
                    easing: 'ease-in-out'
                });

            // Desktop slide shuffle
            } else {
                images.velocity({
                    scaleX: 0.95,
                    scaleY: 0.95
                }, 0);

                n.addClass('next').velocity({
                    scaleX: 1,
                    scaleY: 1
                }, slideshowAnimationDuration);
                c.velocity('stop').velocity({
                    scaleX: 1.05,
                    scaleY: 1.05,
                    translateX: (midMidWidth * 1.075) + 'px',
                    translateY: '-15px'
                }, {
                    complete: function() {
                        n.removeClass('next').addClass('current');
                        c.removeClass('current').velocity({
                            scaleX: 0.95,
                            scaleY: 0.95,
                            translateX: 0,
                            translateY: 0
                        }, {
                            duration: slideshowAnimationDuration,
                            easing: 'ease-in-out'
                        });
                        images.not(n).velocity({
                            scaleX: 0.95,
                            scaleY: 0.95
                        }, 0);
                    },
                    duration: slideshowAnimationDuration,
                    easing: 'ease-in-out'
                });
            }
        }
    });
}

function setSlideHeight() {
    sections.each(function() {
        var t = $(this),
            images = $('.images', t).not('.single'),
            slideHeight = images.eq(0).width() * slideDefaultHeight / slideDefaultWidth;

        images.css({
            height: Math.round(slideHeight) + 'px'
        });
        if(!isMobile) {
            images.find('img:not(.current)').velocity({
                scaleX: 0.95,
                scaleY: 0.95
            }, 0);
        }
    });
}

function scrollToNewSection(e, delta) {
    e.preventDefault();

    // Prevent spamming of this feature
    if(scrollToNewAnimationBusy) return false;

    scrollTop = $(window).scrollTop();
    newScrollTop = Math.round(scrollTop / windowHeight) * windowHeight;

    newScrollTop += delta * windowHeight;

    // We've hit the top
    if(newScrollTop < 0) {
        newScrollTop = 0;
    }

    // We've hit the bottom
    if(newScrollTop > (contentDataLength - 1) * windowHeight) {
        newScrollTop = (contentDataLength - 1) * windowHeight;
    }

    // Animate to newScrollTop if we're not already there
    if(scrollTop !== newScrollTop) {
        // Update section index
        updateSectionIndex();

        scrollToNewAnimationBusy = true;
        body.velocity('stop').velocity('scroll', {
            complete: function() {
                scrollToNewAnimationBusy = false;

                clearInterval(slideshowTimeout);
                slideshowTimeout = setTimeout(slideshowNext, 1000);
            },
            duration: scrollToNewAnimationDuration,
            easing: 'ease-in-out',
            offset: newScrollTop
        });
    }
}