<?php
$content = file_get_contents('content.json');
$content = utf8_encode($content);
$content = json_decode($content);

$domain = $_SERVER['HTTP_HOST'];
$locales = array('en_US');
// $locales = array('en_US', 'de_DE', 'nl_NL');

// switch(str_replace('maikjoosten.', '', $domain)) {
//     case 'com':
//         $currentLang = 'en';
//         $locale = 'en_US';
//         break;
//     case 'de':
//         $currentLang = 'de';
//         $locale = 'de_DE';
//         break;
//     case 'nl':
//         $currentLang = 'nl';
//         $locale = 'nl_NL';
//         break;
//     default:
        $currentLang = 'en';
        $locale = 'en_US';
//         break;
// }
$title = $content->meta->title->$currentLang;
$text = $content->meta->text->$currentLang;
?><!doctype html>
<html lang="<?php echo $currentLang; ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $title; ?></title>
        <meta name="description" content="<?php echo $text; ?>">
        <meta property="og:site_name" content="<?php echo $title; ?>" />
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="<?php echo $text; ?>" />
        <meta property="og:url" content="http://<?php echo $domain; ?>" />
        <meta property="og:image" content="http://<?php echo $domain; ?>/share_image.png"/>
        <meta property="og:image:height" content="630"/>
        <meta property="og:image:width" content="1200"/>
        <meta property="og:locale" content="<?php echo $locale; ?>" />
        <?php
        foreach($locales AS $l) {
            if($l !== $locale) {
                echo '<meta property="og:locale:alternate" content="' . $l . '" />
        ';
            }
        }?>

        <link href="//fonts.googleapis.com/css?family=Josefin+Slab:100,700" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body>
        <div id="language"></div>
        <div id="sectionIndex"></div>
        <div id="sections"></div>

        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="js/velocity.min.js"></script>
        <script type="text/javascript">var currentLang = '<?php echo $currentLang; ?>';</script>
        <script src="js/main.js"></script>
    </body>
</html>